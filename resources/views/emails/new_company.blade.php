@component('mail::message')
# New Company Addition

A new company has been added.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
