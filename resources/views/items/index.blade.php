@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Items List') }}</h3>
        </div>
        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Item Name') }}</th>
                        <th>{{ trans('translation.Price') }}</th>
                        <th>{{ trans('translation.Creation Date') }}</th>
                        <th>{{ trans('translation.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('items'))
                        @php($items = Session('items'))
                    @endif
                    @foreach ($items as $item)
                        <tr>
                            @if (\Session::has('timezone'))
                                @if ($item->created_at != null)
                                    @php($item->created_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->created_at, 'UTC')->setTimezone(Session('timezone')))
                                @endif

                                @if ($item->updated_at != null)
                                    @php($item->updated_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->updated_at, 'UTC')->setTimezone(Session('timezone')))
                                @endif
                            @endif
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->price }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="/items/edit/{{ $item->id }}" class="btn btn-success">Edit
                                    </a>
                                    <a href="/items/delete/{{ $item->id }}"
                                        class="btn btn-danger">{{ trans('translation.Delete') }} </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
