@extends('home')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{trans("translation.Create Item")}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="/items">
            @csrf
            <div class="card-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <div class="form-group">
                    <label for="item_name">{{trans("translation.Item Name")}}</label>
                    <input type="text" class="form-control" id="item_name" name="item_name"
                        placeholder="{{trans("translation.Enter Item Name")}}">
                </div>

                <div class="form-group">
                    <label for="item_price">{{trans("translation.Item Price")}}</label>
                    <input type="number" class="form-control" id="item_price" name="item_price" step="0.01"
                        placeholder="{{trans("translation.Enter Item Price")}}">
                </div>

                <button type="submit" class="btn btn-primary">{{trans("translation.Submit")}}</button>
                <!-- /.card-body -->
        </form>
    </div>
    </div>


@endsection
