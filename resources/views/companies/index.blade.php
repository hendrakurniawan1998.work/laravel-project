@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Filter') }}</h3>
        </div>
        <div class="card-body">
            <form method="post" action="/companies/filter">
                @csrf

                <div class="form-group">
                    <label for="company_name">{{ trans('translation.Company Name') }}</label>
                    <input type="text" class="form-control" id="company_name" name="company_name"
                        placeholder="{{ trans('translation.Enter Company Name') }}" @if (Session::has('company_name'))
                    value="{{ Session::get('company_name') }}"
                    @endif>
                </div>

                <div class="form-group">
                    <label for="creation_date">{{ trans('translation.Creation Date') }}</label>
                    <input type="datetime-local" class="form-control" id="creation_date" name="creation_date"
                        placeholder="{{ trans('translation.Pick A Date') }}" @if (Session::has('company_creation_date'))
                    value="{{ Session::get('company_creation_date') }}"
                    @endif>
                </div>

                <button type="submit" class="btn btn-primary">{{ trans('translation.Filter') }}</button>
                <button type="reset" onclick="window.location=`{{ url('/clear-companies-filter') }}`"
                    class="btn btn-danger">{{ trans('translation.Clear Filter') }}</button>
            </form>
            <!-- /.card-body -->
        </div>
        <div class="card-body">
            <form method="post" action="/timezone">
                @csrf
                <div class="form-group">
                    <label for="timezone">{{ trans('translation.View As Timezone') }}</label>
                    <select class="form-control" id="timezone" name="timezone">
                        <option value="" hidden>{{ trans('translation.Pick A Timezone') }}</option>
                        @foreach ($tzlist as $tz)
                            @if (\Session::has('timezone'))
                                @if ($tz == Session::get('timezone'))
                                    <option value={{ $tz }} selected>{{ $tz }}</option>
                                @else
                                    <option value={{ $tz }}>{{ $tz }}</option>
                                @endif
                            @else
                                <option value={{ $tz }}>{{ $tz }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Change Timezone') }}</button>
            </form>
        </div>
        <!-- /.card-body -->

    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Companies List') }}</h3>
        </div>

        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>
            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Name') }}</th>
                        <th>{{ trans('translation.Email Address') }}</th>
                        <th>Logo</th>
                        <th>{{ trans('translation.Website') }}</th>
                        <th>{{ trans('translation.Created Date') }}</th>
                        <th>{{ trans('translation.Updated Date') }}</th>
                        <th>{{ trans('translation.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('companies'))
                        @php($companies = Session('companies'))
                    @endif
                    @foreach ($companies as $company)
                        @if (\Session::has('timezone'))
                            @if ($company->created_at != null)
                                @php($company->created_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $company->created_at, 'UTC')->setTimezone(Session('timezone')))
                            @endif

                            @if ($company->updated_at != null)
                                @php($company->updated_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $company->updated_at, 'UTC')->setTimezone(Session('timezone')))
                            @endif
                        @endif
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->email }}</td>
                            @if (empty($company->logo))
                                <td>{{ trans('translation.No Logo') }}</td>
                            @else
                                <td> <img src="{{ asset("storage/$company->logo") }}" width="100" height="100" />
                                </td>
                            @endif
                            <td>{{ $company->website }}</td>
                            <td>{{ $company->created_at }}</td>
                            <td>{{ $company->updated_at }}</td>

                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="/companies/edit/{{ $company->id }}" class="btn btn-success">Edit
                                    </a>
                                    <a href="/companies/delete/{{ $company->id }}" class="btn btn-danger">Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, -1],
                        [10, 25, 50, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
