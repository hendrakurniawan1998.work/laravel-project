@extends('home')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{trans("translation.Create Company")}}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="/companies" enctype="multipart/form-data">
            @csrf
            <div class="card-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <div class="form-group">
                    <label for="company_name">{{trans("translation.Name")}}</label>
                    <input type="text" class="form-control" id="company_name" name="company_name"
                        placeholder="{{trans("translation.Company Name")}}" value="{{ old('company_name') }}">
                </div>

                <div class="form-group">
                    <label for="company_email">{{trans("translation.Email Address")}}</label>
                    <input type="email" class="form-control" id="company_email" name="company_email"
                        placeholder="{{trans("translation.Enter Email Address")}}"  value="{{ old('company_email') }}">
                </div>


                <div class="form-group">
                    <label for="company_website">{{trans("translation.Website")}}</label>
                    <input type="text" class="form-control" id="company_website" name="company_website"
                        placeholder="{{trans("translation.Enter Website")}}"  value="{{ old('company_website') }}">
                </div>

                <div class="form-group">
                  <label for="company_logo">Logo</label>
                  <input class="form-control" type="file" id="company_logo" name="company_logo" >
              </div>

                <button type="submit" class="btn btn-primary">{{trans("translation.Submit")}}</button>
                <!-- /.card-body -->
        </form>
    </div>
    </div>


@endsection
