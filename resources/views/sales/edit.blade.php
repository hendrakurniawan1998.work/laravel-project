@extends('home')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Edit Sale') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->

        <form method="post" action="/sales/update/{{ $sale->id }}/">
            @csrf
            <div class="card-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <div class="form-group">
                    <label for="sale_item_id">{{ trans('translation.Sale Item') }}</label>
                    <select class="form-control" id="sale_item_id" name="sale_item_id">
                        @foreach ($items as $item)
                            @if ($item->id == $sale->item_id)
                                <option value={{ $item->id }} selected>{{ $item->name }} - Rp.{{ $item->price }}</option>
                            @else
                                <option value={{ $item->id }}>{{ $item->name }} - Rp.{{ $item->price }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="sale_discount">{{ trans('translation.Sale Discount') }}</label>
                    <input type="number" class="form-control" id="sale_discount" name="sale_discount" step='0.01' min=0 max=100
                        value="{{ $sale->discount * 100 }}">
                </div>

                <div class="form-group">
                    <label for="sale_employee_id">{{ trans('translation.Employee') }}</label>
                    <select class="form-control" id="sale_employee_id" name="sale_employee_id">
                        @foreach ($employees as $employee)
                            @if ($employee->id == $sale->employee_id)
                                <option value={{ $employee->id }} selected>{{ $employee->first_name }}
                                    {{ $employee->last_name }}</option>
                            @else
                                <option value={{ $employee->id }}>{{ $employee->first_name }}
                                    {{ $employee->last_name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">{{ trans('translation.Submit') }}</button>
                <!-- /.card-body -->
        </form>
    </div>
    </div>


@endsection
