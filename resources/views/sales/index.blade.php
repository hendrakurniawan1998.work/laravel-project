@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Sales List') }}</h3>
        </div>
        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Creation Date') }}</th>
                        <th>{{ trans('translation.Sale Item') }}</th>
                        <th>{{ trans('translation.Sale Price') }}</th>
                        <th>{{ trans('translation.Sale Discount') }}</th>
                        <th>{{ trans('translation.Sale Employee (ID)') }}</th>
                        <th>{{ trans('translation.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('sales'))
                        @php($sales = Session('sales'))
                    @endif
                    @foreach ($sales as $sale)
                        <tr>
                            <td>{{ $sale->created_at }}</td>
                            <td>{{ $sale->item->name }}</td>
                            <td>{{  str_replace('.00', '', number_format($sale->price, 2, '.', ',')) }}</td>
                            <td>{{ $sale->discount* 100 + 0 }}%</td>
                            <td>{{ $sale->employee->first_name }} {{ $sale->employee->last_name }} ({{ $sale->employee_id }})</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="/sales/edit/{{ $sale->id }}" class="btn btn-success">Edit
                                    </a>
                                    <a href="/sales/delete/{{ $sale->id }}"
                                        class="btn btn-danger">{{ trans('translation.Delete') }} </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
