@extends('home')

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Create Employee') }}</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->



        <form method="post" action="/employees">
            @csrf
            <div class="card-body">

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ \Session::get('success') }}</p>
                    </div>
                @endif

                <div class="form-group">
                    <label for="employee_name">{{ trans('translation.First Name') }}</label>
                    <input type="text" class="form-control" id="employee_first_name" name="employee_first_name"
                        placeholder="{{ trans('translation.Employee First Name') }}" value="{{ old('employee_first_name') }}">
                </div>


                <div class="form-group">
                    <label for="employee_name">{{ trans('translation.Last Name') }}</label>
                    <input type="text" class="form-control" id="employee_last_name" name="employee_last_name"
                        placeholder="{{ trans('translation.Employee Last Name') }}" value="{{ old('employee_last_name') }}">
                </div>

                <div class="form-group">
                    <label for="employee_company">{{ trans('translation.Company') }}</label>
                    <select class="form-control" id="employee_company" name="employee_company">
                        <option value="" hidden>{{ trans('translation.Pick A Company') }}</option>
                        @foreach ($companies as $company)
                            @if (old('employee_company') == $company->id )
                                <option value="{{ $company->id  }}" selected>{{ $company->id }}. {{ $company->name }}</option>
                            @else
                            <option value={{ $company->id }}>{{ $company->id }}. {{ $company->name }}</option>
                            @endif
                         
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="employee_email">{{ trans('translation.Email Address') }}</label>
                    <input type="email" class="form-control" id="employee_email" name="employee_email"
                        placeholder="{{ trans('translation.Enter Email Address') }}" value="{{ old('employee_email') }}">
                </div>

                <div class="form-group">
                    <label for="employee_phone">{{ trans('translation.Phone') }}</label>
                    <input type="text" class="form-control" id="employee_phone" name="employee_phone"
                        placeholder="{{ trans('translation.Enter Phone Number') }}" value="{{ old('employee_phone') }}">
                </div>

                <div class="form-group">
                    <label for="employee_password">{{ trans('translation.Password') }}</label>
                    <input type="password" class="form-control" id="employee_password" name="employee_password"
                        placeholder="{{ trans('translation.Enter Password') }}" >
                </div>

                <button type="submit" class="btn btn-primary">{{ trans('translation.Submit') }}</button>
                <!-- /.card-body -->
        </form>
    </div>
    </div>


@endsection
