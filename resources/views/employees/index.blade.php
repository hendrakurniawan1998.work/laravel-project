@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Filter') }}</h3>
        </div>

        <div class="card-body">
            <form method="post" action="employees/filter">
                @csrf
                <div class="form-group">
                    <label for="employee_first_name">{{ trans('translation.First Name') }}</label>

                    <input type="text" class="form-control" id="employee_first_name" name="employee_first_name"
                        placeholder="{{ trans('translation.Employee First Name') }}" @if (Session::has('employee_first_name'))
                    value ="{{ session('employee_first_name') }}""
                    @endif>

                </div>


                <div class="form-group">
                    <label for="employee_company">{{ trans('translation.Company') }}</label>
                    <select class="form-control" id="employee_company" name="employee_company">
                        <option value="" selected="false" hidden>{{ trans('translation.Pick A Company') }}</option>
                        @foreach ($companies as $company)
                            @if (Session::has('employee_company'))
                                @if ($company->id ==  Session::get('employee_company'))
                                    <option value={{ $company->id }} selected>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>
                                @else
                                    <option value={{ $company->id }}>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>
                                @endif

                            @else
                                <option value={{ $company->id }}>{{ $company->id }}.
                                    {{ $company->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="creation_date">{{ trans('translation.Creation Date') }}</label>
                    <input type="datetime-local" class="form-control" id="creation_date" name="creation_date"
                        placeholder="{{ trans('translation.Pick A Date') }}" @if (Session::has('employee_creation_date'))
                    value="{{  Session::get('employee_creation_date') }}"
                    @endif>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Filter') }}</button>
               <button type="reset" onclick="window.location=`{{ url('/clear-employees-filter') }}`" class="btn btn-danger">{{ trans('translation.Clear Filter') }}</button>
            </form>

            <!-- /.card-body -->
        </div>
        <div class="card-body">
            <form method="post" action="/timezone">
                @csrf
                <div class="form-group">
                    <label for="timezone">{{ trans('translation.View As Timezone') }}</label>
                    <select class="form-control" id="timezone" name="timezone">
                        <option value="" hidden>{{ trans('translation.Pick A Timezone') }}</option>
                        @foreach ($tzlist as $tz)
                            @if (Session::has('timezone'))
                                @if ($tz == Session::get('timezone'))
                                    <option value={{ $tz }} selected>{{ $tz }}</option>
                                @else
                                    <option value={{ $tz }}>{{ $tz }}</option>
                                @endif

                            @else
                                <option value={{ $tz }}>{{ $tz }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Change Timezone') }}</button>
            </form>
            <!-- /.card-body -->
        </div>
    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Employees List') }}</h3>
        </div>

        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.First Name') }}</th>
                        <th>{{ trans('translation.Last Name') }}</th>
                        <th>{{ trans('translation.Company') }}</th>
                        <th>{{ trans('translation.Email Address') }}</th>
                        <th>{{ trans('translation.Phone') }}</th>
                        <th>{{ trans('translation.Created Date') }}</th>
                        <th>{{ trans('translation.Updated Date') }}</th>
                        <th>{{ trans('translation.Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        @if (\Session::has('employees'))
                            @php($employees = Session::get('employees'))
                        @endif
                        @foreach ($employees as $employee)
                            @if (\Session::has('timezone'))
                                @if ($employee->created_at != null)
                                    @php($employee->created_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $employee->created_at, 'UTC')->setTimezone(Session('timezone')))
                                @endif

                                @if ($employee->updated_at != null)
                                    @php($employee->updated_at = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $employee->updated_at, 'UTC')->setTimezone(Session('timezone')))
                                @endif
                            @endif
                            <td>{{ $employee->first_name }}</td>
                            <td>{{ $employee->last_name }}</td>
                            <td>{{ $employee->company->name }}</td>
                            <td>{{ $employee->email }}</td>
                            <td>{{ $employee->phone }}</td>
                            <td>{{ $employee->created_at }}</td>
                            <td>{{ $employee->updated_at }}</td>
                            <td>
                                <div class="btn-group btn-group-sm" role="group">
                                    <a href="/employees/edit/{{ $employee->id }}" class="btn btn-success">Edit
                                    </a>
                                    <a href="/employees/delete/{{ $employee->id }}"
                                        class="btn btn-danger">{{ trans('translation.Delete') }} </a>
                                </div>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
