@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Summary Date') }}</h3>
        </div>

        <div class="card-body">
            <form method="GET" action="/daily-sales-summaries">
                <div class="form-group">
                    <label for="summary_date">{{ trans('translation.Date') }}</label>
                    <input type="date" class="form-control" id="summary_date" name="summary_date">
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Change Date') }}</button>
            </form>

            <!-- /.card-body -->
        </div>

        <div class="card-body">
            <form method="post" action="/timezone">
                @csrf
                <div class="form-group">
                    <label for="timezone">{{ trans('translation.View As Timezone') }}</label>
                    <select class="form-control" id="timezone" name="timezone">
                        <option value="" hidden>{{ trans('translation.Pick A Timezone') }}</option>
                        @foreach ($tzlist as $tz)
                            @if (\Session::has('timezone'))
                                @if ($tz == session('timezone'))
                                    <option value={{ $tz }} selected>{{ $tz }}</option>
                                @else
                                    <option value={{ $tz }}>{{ $tz }}</option>
                                @endif
                            @else
                                <option value={{ $tz }}>{{ $tz }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Change Timezone') }}</button>
            </form>
        </div>
        <!-- /.card-body -->

    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Daily Sales Summaries List') }}</h3>
        </div>
        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Date') }}</th>
                        <th>{{ trans('translation.Employee') }}</th>
                        <th>{{ trans('translation.Created Date') }}</th>
                        <th>{{ trans('translation.Last Update') }}</th>
                        <th>{{ trans('translation.Total Price') }}</th>
                        <th>{{ trans('translation.Total Discount') }}</th>
                        <th>{{ trans('translation.Total') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('sales_summaries'))
                        @php($sales_summaries = Session('sales_summaries'))
                    @endif
                    @foreach ($sales_summaries as $sales_summary)
                        @if (\Session::has('timezone'))

                        @endif
                        <tr>
                            <td>{{ $sales_summary->date }}</td>
                            <td>{{ $sales_summary->employee_id }}</td>
                            <td>{{ $sales_summary->created_date }}</td>
                            <td>{{ $sales_summary->last_update }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->price_total, 2, '.', ',')) }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->discount_total, 2, '.', ',')) }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->total, 2, '.', ',')) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
