@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Summary Filter') }}</h3>
        </div>

        <div class="card-body">
            <form method="post" action="/sales_summaries/filter">
                @csrf
                <div class="form-group">
                    <label for="summary_start_date">{{ trans('translation.Summary Start Date') }}</label>
                    @if (session()->has('summary_start_date'))
                        <input type="date" class="form-control" id="summary_start_date" name="summary_start_date"
                            value={{ session(summary_end_date) }}>
                    @else
                        <input type="date" class="form-control" id="summary_start_date" name="summary_start_date">
                    @endif
                </div>


                <div class="form-group">
                    <label for="summary_end_date">{{ trans('translation.Summary End Date') }}</label>
                    @if (session()->has('summary_end_date'))
                        <input type="date" class="form-control" id="summary_end_date" name="summary_end_date"
                            value={{ session(summary_end_date) }}>
                    @else
                        <input type="date" class="form-control" id="summary_end_date" name="summary_end_date">
                    @endif
                </div>


                <div class="form-group">
                    <label for="summary_company">{{ trans('translation.Summary Company') }}</label>
                    <select class="form-control" id="summary_company" name="summary_company">
                        <option value="" selected="false" hidden>{{ trans('translation.Pick A Company') }}</option>
                        @foreach ($companies as $company)
                            @if (session()->has('summary_company'))
                                @if ($company->id == session('summary_company'))
                                    <option value={{ $company->id }} selected>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>

                                @else
                                    <option value={{ $company->id }}>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>
                                @endif

                            @else
                                <option value={{ $company->id }}>{{ $company->id }}.
                                    {{ $company->name }}
                                </option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="summary_employee">{{ trans('translation.Summary Employee') }}</label>
                    <select class="form-control" id="summary_employee" name="summary_employee">
                        <option value="" selected="false" hidden>{{ trans('translation.Pick An Employee') }}</option>
                        @foreach ($employees as $employee)
                            @if (session()->has('summary_employee'))
                                @if ($employee->id == session('summary_employee'))
                                    <option value={{ $employee->id }} selected>{{ $employee->id }}.
                                        {{ $employee->first_name }}
                                        {{ $employee->last_name }}</option>

                                @else
                                    <option value={{ $employee->id }}>{{ $employee->id }}.
                                        {{ $employee->first_name }}
                                        {{ $employee->last_name }}</option>
                                @endif


                            @else
                                <option value={{ $employee->id }}>{{ $employee->id }}.
                                    {{ $employee->first_name }}
                                    {{ $employee->last_name }}</option>
                            @endif

                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Filter') }}</button>
            </form>

            <!-- /.card-body -->
        </div>

    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Sales Summaries List') }}</h3>
        </div>
        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table" id="datatable">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Date') }}</th>
                        <th>{{ trans('translation.Employee ID') }}</th>
                        <th>{{ trans('translation.Created Date') }}</th>
                        <th>{{ trans('translation.Last Update') }}</th>
                        <th>{{ trans('translation.Total Price') }}</th>
                        <th>{{ trans('translation.Total Discount') }}</th>
                        <th>{{ trans('translation.Total') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('sales_summaries'))
                        @php($sales_summaries = Session('sales_summaries'))
                    @endif
                    @foreach ($sales_summaries as $sales_summary)
                        @if (\Session::has('timezone'))

                        @endif
                        <tr>
                            <td><a href="/sales">{{ $sales_summary->date }}</a></td>
                            <td>{{ $sales_summary->employee_id }}</td>
                            <td>{{ $sales_summary->created_date }}</td>
                            <td>{{ $sales_summary->last_update }}</td>
                            <td>{{ $sales_summary->price_total }}</td>
                            <td>{{ $sales_summary->discount_total }}</td>
                            <td>{{ $sales_summary->total }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
