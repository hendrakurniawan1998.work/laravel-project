@extends('home')

@section('styles')
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Summary Filter') }}</h3>
        </div>

        <div class="card-body">
            <form method="post" action="/sales-summaries/filter">
                @csrf
                <div class="form-group">
                    <label for="summary_start_date">{{ trans('translation.Summary Start Date') }}</label>
                    <input type="date" class="form-control" id="summary_start_date" name="summary_start_date" @if (Session::has('summary_start_date'))
                    value={{ Session::get('summary_start_date') }}
                    @endif>

                </div>

                <div class="form-group">
                    <label for="summary_end_date">{{ trans('translation.Summary End Date') }}</label>
                    <input type="date" class="form-control" id="summary_end_date" name="summary_end_date" @if (Session::has('summary_end_date'))
                    value={{ Session::get('summary_end_date') }}
                    @endif>

                </div>

                <div class="form-group">
                    <label for="summary_company">{{ trans('translation.Summary Company') }}</label>
                    <select class="form-control" id="summary_company" name="summary_company">
                        <option value="" selected="false" hidden>{{ trans('translation.Pick A Company') }}</option>
                        @foreach ($companies as $company)
                            @if (Session::has('summary_company'))
                                @if ($company->id == Session::get('summary_company'))
                                    <option value={{ $company->id }} selected>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>
                                @else
                                    <option value={{ $company->id }}>{{ $company->id }}.
                                        {{ $company->name }}
                                    </option>
                                @endif
                            @else
                                <option value={{ $company->id }}>{{ $company->id }}.
                                    {{ $company->name }}
                                </option>
                            @endif


                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="summary_employee">{{ trans('translation.Summary Employee') }}</label>
                    <select class="form-control" id="summary_employee" name="summary_employee">
                        <option value="" selected="false" hidden>{{ trans('translation.Pick An Employee') }}</option>
                        @foreach ($employees as $employee)
                            @if (Session::has('summary_employee'))
                                @if ($employee->id == Session::get('summary_employee'))
                                    <option value={{ $employee->id }} selected>{{ $employee->id }}.
                                        {{ $employee->first_name }}
                                        {{ $employee->last_name }}</option>
                                @else
                                    <option value={{ $employee->id }}>{{ $employee->id }}.
                                        {{ $employee->first_name }}
                                        {{ $employee->last_name }}</option>
                                @endif
                            @else
                                <option value={{ $employee->id }}>{{ $employee->id }}.
                                    {{ $employee->first_name }}
                                    {{ $employee->last_name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Filter') }}</button>
                <button type="reset" onclick="window.location=`{{ url('/clear-summaries-filter') }}`"
                    class="btn btn-danger">{{ trans('translation.Clear Filter') }}</button>
            </form>
            <!-- /.card-body -->
        </div>

        <div class="card-body">
            <form method="post" action="/timezone">
                @csrf
                <div class="form-group">
                    <label for="timezone">{{ trans('translation.View As Timezone') }}</label>
                    <select class="form-control" id="timezone" name="timezone">
                        <option value="" hidden>{{ trans('translation.Pick A Timezone') }}</option>
                        @foreach ($tzlist as $tz)
                            @if (\Session::has('timezone'))
                                @if ($tz == Session::get('timezone'))
                                    <option value={{ $tz }} selected>{{ $tz }}</option>
                                @else
                                    <option value={{ $tz }}>{{ $tz }}</option>
                                @endif
                            @else
                                <option value={{ $tz }}>{{ $tz }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">{{ trans('translation.Change Timezone') }}</button>
            </form>
        </div>
        <!-- /.card-body -->

    </div>
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">{{ trans('translation.Sales Summaries List') }}</h3>
        </div>
        <div class="card-body">
            @if (Session::has('success'))

                <div class="alert alert-success" role="alert">
                    {{ Session::get('success') }}
                </div>

            @endif
            <table class="table">
                <thead>
                    <tr>
                        <th>{{ trans('translation.Date') }}</th>
                        <th>{{ trans('translation.Employee') }}</th>
                        <th>{{ trans('translation.Created Date') }}</th>
                        <th>{{ trans('translation.Last Update') }}</th>
                        <th>{{ trans('translation.Total Price') }}</th>
                        <th>{{ trans('translation.Total Discount') }}</th>
                        <th>{{ trans('translation.Total') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @if (\Session::has('sales_summaries'))
                        @php($sales_summaries = Session('sales_summaries'))
                    @endif
                    @foreach ($sales_summaries as $sales_summary)
                        @if (\Session::has('timezone'))
                            @if ($sales_summary->created_date != null)
                                @php($sales_summary->created_date = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sales_summary->created_date, 'UTC')->setTimezone(Session::get('timezone')))
                            @endif

                            @if ($sales_summary->last_update != null)
                                @php($sales_summary->last_update = Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $sales_summary->last_update, 'UTC')->setTimezone(Session::get('timezone')))
                            @endif
                        @endif
                        <tr>
                            <td><a
                                    href="/sales-summaries/detail/{{ $sales_summary->date }}">{{ $sales_summary->date }}</a>
                            </td>
                            <td>{{ $sales_summary->employee->first_name}} {{ $sales_summary->employee->last_name}} ({{ $sales_summary->employee_id}})</td>
                            <td>{{ $sales_summary->created_date }}</td>
                            <td>{{ $sales_summary->last_update }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->price_total, 2, '.', ',')) }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->discount_total, 2, '.', ',')) }}</td>
                            <td>{{ str_replace('.00', '', number_format($sales_summary->total, 2, '.', ',')) }}</td>
                        </tr>
                    @endforeach
                    {{ $sales_summaries->links() }}
                </tbody>
            </table>
        </div>
    </div>
@endsection


@section('javascripts')
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    @if (config('app.locale') == 'id')
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.11.3/i18n/id.json'
                    },

                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @else
        <script>
            $(document).ready(function() {
                $('#datatable').DataTable({
                    "lengthMenu": [
                        [10, 25, 50, 75, 100, -1],
                        [10, 25, 50, 75, 100, "All"]
                    ]
                });
            });
        </script>
    @endif

@endsection
