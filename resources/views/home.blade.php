<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">



    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @yield('styles')

    <title>{{ "PT Agung Trisula Mandiri's Laravel Project" }}</title>



</head>

<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">

            <!-- Sidebar -->
            <div class="sidebar">

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">

                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                        <li class="nav-item has-treeview menu-open">
                            <a href="/home" class="nav-link active">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    {{ trans('translation.Home') }}
                                </p>
                            </a>

                            <!-- Nav Item Employee -->
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-male"></i>
                                <p>
                                    {{ trans('translation.Employee') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <!-- Employee Tree Menu -->
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/employees" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Employees List') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/employees/create" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>{{ trans('translation.Add Employee') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <!-- Nav Item Company -->
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-briefcase"></i>
                                <p>
                                    {{ trans('translation.Company') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <!-- Company Tree Menu -->
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/companies" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Companies List') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/companies/create" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>{{ trans('translation.Add Company') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-spray-can"></i>
                                <p>
                                    {{ trans('translation.Item') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <!-- Item Tree Menu -->
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/items" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Items List') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/items/create" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>{{ trans('translation.Add Item') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-dollar-sign"></i>
                                <p>
                                    {{ trans('translation.Sale') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <!-- Sales Tree Menu -->
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/sales" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Sales List') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/sales/create" class="nav-link">
                                        <i class="nav-icon fas fa-plus"></i>
                                        <p>{{ trans('translation.Add Sale') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>

                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-dollar-sign"></i>
                                <p>
                                    {{ trans('translation.Sales Summaries') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>

                            <!-- Sales Tree Menu -->
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="/daily-sales-summaries" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Daily Sales Summaries List') }}</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="/sales-summaries" class="nav-link">
                                        <i class="nav-icon fas fa-list"></i>
                                        <p>{{ trans('translation.Sales Summaries List') }}</p>
                                    </a>
                                </li>
                            </ul>
                        </li>


                        <li class="nav-item">
                            <a href="{{ route('logout-user') }}" class="nav-link">
                                <i class="nav-icon fas fa-table"></i>
                                <p>
                                    {{ trans('translation.Logout') }}
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                @yield('content')
            </div>

            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            @include('partials/language-switcher')
        </footer>
    </div>
    <!-- ./wrapper -->


    <script src="{{ asset('js/app.js') }}"></script>


    @yield('javascripts')




</body>

</html>
