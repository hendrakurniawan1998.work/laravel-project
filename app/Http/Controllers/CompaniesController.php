<?php

namespace App\Http\Controllers;

use App\Mail\QueueEmail;
use Illuminate\Http\Request;
use App\Models\Companies;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Carbon;
use DateTimeZone;

class CompaniesController extends Controller
{

    public function __construct()
    {
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        $companies = Companies::all();

        return view('companies.index',compact('tzlist','companies'));
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name'    =>  'required',
            'company_email'    =>  'required',
            'company_website'    =>  'required',
            'company_logo' => 'image|dimensions:min_width:100,min_height:100'
        ]);

        $company = new Companies();
        $company->name=$request->input('company_name');
        $company->email=$request->input('company_email');
        $company->website=$request->input('company_website');



        if($request->hasFile('company_logo'))
        {
            $company->logo = $request->file('company_logo')->store('company_logo');
        }

        $company->created_at = Carbon::now();
        $company->created_by_id = session()->get('UserID');


        if($company->save())
        {
            $mail = Mail::to('hendra.kurniawan1998@gmail.com')->queue(new QueueEmail());
           
            return back()->with('success', 'Data Added');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Companies::find($id);
        return view("companies.edit", compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name'    =>  'required',
            'company_email'    =>  'required',
            'company_website'    =>  'required',
            'company_logo'    =>  'image|dimensions:min-width:100,min-height:100'
        ]);

        $company = Companies::find($id);
        $company->name=$request->input('company_name');
        $company->email=$request->input('company_email');
        $company->website=$request->input('company_website');


        if($request->hasFile("company_logo"))
        {
            if($company->logo)
            {
                Storage::delete($company->logo);
            }

            $company->logo = $request->file('company_logo')->store('company_logo');
            
        }

        $company->updated_at=Carbon::now();

        $company->updated_by_id = session()->get('UserID');

        if($company->update())
        {
            return redirect()->back()->with('success','Company Information Updated Successfully');
        }
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company=Companies::find($id);

        if($company->logo)
        {
            Storage::delete($company->logo);
        }

        $company->delete();

        return back()->with('success','Company has been deleted successfully');

    }
}
