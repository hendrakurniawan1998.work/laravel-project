<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sales;
use App\Models\Items;
use App\Models\Employees;
use App\Http\Controllers\SalesSummariesController;
use App\Models\SalesSummaries;
use Illuminate\Support\Facades\Date;
use DateTime;
use Illuminate\Support\Carbon;

use DateTimeZone;

class SalesController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sales::all();

        return view('Sales.index', compact('sales'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = Employees::all();
        $items = Items::all();
        return view('sales.create', compact('employees', 'items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sale_item_id'    =>  'required|exists:items,id',
            'sale_discount'    =>  'required|max:100|min:0',
            'sale_employee_id' => 'required|exists:employees,id',
        ]);

        $sale = new Sales();
        $sale->item_id = $request->input('sale_item_id');
        $sale->price = Items::find($sale->item_id)->price;
        $sale->discount = $request->input('sale_discount') / 100;
        $sale->employee_id = $request->input('sale_employee_id');
        $sale->created_at = Carbon::now()->toDateTimeString();

        if ($sale->save()) {

            $summary = new SalesSummariesController();
            $summary_id = $summary->exist($sale->created_at, $sale->employee_id);

            if ($summary_id != 0) {
                $summary->add($sale, $summary_id);
            } else {
                $summary->store($sale);
            }

            return back()->with('success', 'Data Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sales::find($id);
        $employees = Employees::all();
        $items = Items::all();
        return view("Sales.edit", compact('sale', 'employees', 'items'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'sale_item_id'    =>  'required|exists:items,id',
            'sale_discount'    =>  'required',
            'sale_employee_id' => 'required|exists:employees,id',
        ]);

        $sale = Sales::find($id);
        $sale->item_id = $request->input('sale_item_id');
        $old_sale_discount =   $sale->discount;
        $sale->discount = $request->input('sale_discount')/100;
        $sale->employee_id = $request->input('sale_employee_id');

        if ($sale->update()) {

            $sale->discount = ($sale->discount - $old_sale_discount);

            error_log($sale->discount);
            error_log($sale->created_at);
            error_log($sale->employee_id);
        

            $summary = new SalesSummariesController();
            $summary_id = $summary->exist($sale->created_at, $sale->employee_id);

            error_log($summary_id);

            if ($summary_id != 0) {
                $summary->update($sale, $summary_id);
            }

            return redirect()->back()->with('success', 'Sale Information Updated Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sales::find($id);
        if ($sale->delete()) {

            $summary = new SalesSummariesController();
            $summary_id = $summary->exist($sale->created_at, $sale->employee_id);

            if ($summary_id != 0) {
                $summary->deduct($sale, $summary_id);
            }
            return back()->with('success', 'Sale has been deleted successfully');
        }
    }
}
