<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TimezoneController extends Controller
{
    public function changeTime(Request $request)
    {
        $this->validate($request, [
            'timezone'    =>  'required',
        ]);

        // Search for a employee based on their first name.
        if ($request->has('timezone') && $request->timezone != null) {
            $request->session()->put('timezone',$request->timezone);
            return back();
        }
    }
}
