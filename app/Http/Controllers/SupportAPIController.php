<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;

class SupportAPIController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'logout']]);
    }

    public function RetrieveEmployees(Request $request)
    {
        if ($request->has('company_id') && $request->company_id != null) {

            $employees = Employees::where("company_id", "=", $request->company_id)->get();

            return response()->json($employees);
        }

        return response()->json("Invalid Company ID");
    }
}
