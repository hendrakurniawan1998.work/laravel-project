<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Sales;
use App\Models\Companies;
use App\Models\SalesSummaries;
use DateTimeZone;
use Illuminate\Support\Facades\Date;

class SalesSummariesController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $companies=Companies::all();
        $employees=Employees::all();
        $sales_summaries = SalesSummaries::simplepaginate(10);
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        return view('sales_summaries.index', compact('sales_summaries','employees','companies','tzlist'));
    }

    public function daily(Request $request)
    {
        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        if($request->has("summary_date"))
        {
            $sales_summaries = SalesSummaries::all()->where('date',$request->input('summary_date'));
        }
        else
        {
            $sales_summaries =SalesSummaries::all()->where('date',Carbon::now()->toDateString());
        }

        return view('sales_summaries.daily', compact('sales_summaries','tzlist'));
    }

    public function exist(string $date, string $employee_id)
    {
        $time = strtotime($date);

        $result = SalesSummaries::where('date', date('Y-m-d',$time))->where('employee_id', $employee_id)->first();

        if ($result != null) {
            return $result->id;
        }

        return 0;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Sales $sale)
    {
        $sales_summary = new SalesSummaries();
        $sales_summary->date = $sale->created_at;
        $sales_summary->employee_id = $sale->employee_id;
        $sales_summary->created_date =Carbon::now()->toDateTime();
        $sales_summary->price_total = $sale->price;
        $sales_summary->discount_total = $sale->discount * $sale->price;
        $sales_summary->total = $sales_summary->price_total - $sales_summary->discount_total;

        if (!$sales_summary->save()) {
            error_log("ERROR: Summary Not Created");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Sales $sale, $id)
    {

        $sales_summary = SalesSummaries::find($id);

        $sales_summary->last_update = Carbon::now()->toDateTime();
        $sales_summary->discount_total += $sale->discount * $sale->price;
        $sales_summary->total =   $sales_summary->price_total - $sales_summary->discount_total;

        if (!$sales_summary->update()) {
            error_log("ERROR: Summary Not Updated");
        }
    }

    public function add(Sales $sale, $id)
    {

        $sales_summary = SalesSummaries::find($id);

        $sales_summary->last_update = Carbon::now()->toDateTime();
        $sales_summary->price_total += $sale->price;
        $sales_summary->discount_total += $sale->discount * $sale->price;
        $sales_summary->total =   $sales_summary->price_total - $sales_summary->discount_total;

        if (!$sales_summary->update()) {
            error_log("ERROR: Summary Not Updated");
        }
    }

    public function deduct(Sales $sale, $id)
    {

        $sales_summary = SalesSummaries::find($id);

        $sales_summary->last_update = Carbon::now()->toDateTime();
        $sales_summary->price_total -=  $sale->price;
        $sales_summary->discount_total -=  $sale->discount * $sale->price;
        $sales_summary->total =   $sales_summary->price_total - $sales_summary->discount_total;

        if( $sales_summary->price_total==0)
        {
            $this->destroy($sales_summary);
        }

        if (!$sales_summary->update()) {
            error_log("ERROR: Summary Not Updated");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  SalesSummaries  $sales_summary
     */
    public function destroy(SalesSummaries $sales_summary)
    {
        if (!$sales_summary->delete()) {
            error_log("ERROR: Summary Not Deleted");
        }
    }
}
