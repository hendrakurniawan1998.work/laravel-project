<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Items;
use Illuminate\Support\Carbon;
use DateTimeZone;

class ItemsController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = items::all();

        return view('items.index',compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('items.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'item_name'    =>  'required',
            'item_price'    =>  'required',
          
        ]);

        $item = new Items();
        $item->name = $request->input('item_name');
        $item->price = $request->input('item_price');
        $item->created_at = Carbon::now();

        if ($item->save()) {
            return back()->with('success', 'Data Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Items::find($id);
        return view("items.edit", compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'item_name'    =>  'required',
            'item_price'    =>  'required',
        ]);


        $item = Items::find($id);
        
        $item->name = $request->input('item_name');
        $item->price = $request->input('item_price');

        $item->updated_at = Carbon::now();

        if ($item->update()) {
            return redirect()->back()->with('success', 'Item Information Updated Successfully');
        }
        else
        {
            return redirect()->back()->with('error', 'Bro');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Items::find($id)->delete()) {
            return back()->with('success', 'Item has been deleted successfully');
        }
    }
}
