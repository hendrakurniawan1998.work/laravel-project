<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employees;
use App\Models\Companies;
use Illuminate\Support\Carbon;
use DateTimeZone;

class EmployeesController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employees::all();

        $companies = Companies::all();

        $tzlist = DateTimeZone::listIdentifiers(DateTimeZone::ALL);

        return view('employees.index',compact('employees','companies','tzlist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $companies = Companies::all();

        return view('employees.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'employee_first_name'    =>  'required',
            'employee_last_name'    =>  'required',
            'employee_company' => 'exists:companies,id',
            'employee_password'    =>  'required',
        ]);

        $employee = new Employees();
        
        $employee->first_name = $request->input('employee_first_name');
        $employee->last_name = $request->input('employee_last_name');
        $employee->company_id = $request->input('employee_company');
        $employee->email = $request->input('employee_email');
        $employee->phone = $request->input('employee_phone');
        $employee->password = $request->input('employee_password');

        $employee->created_at = Carbon::now();
        $employee->created_by_id = session()->get('UserID');
        


        if ($employee->save()) {
            return back()->with('success', 'Data Added');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employees::find($id);
        
        $companies = Companies::all();

        return view("employees.edit", compact('employee','companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'employee_first_name'    =>  'required',
            'employee_last_name'    =>  'required',
            'employee_company' => 'exists:companies,id'
        ]);


        $employee = Employees::find($id);

        $employee->first_name = $request->input('employee_first_name');
        $employee->last_name = $request->input('employee_last_name');
        $employee->company_id = $request->input('employee_company');
        $employee->email = $request->input('employee_email');
        $employee->phone = $request->input('employee_phone');

        $employee->updated_at = Carbon::now();
        $employee->updated_by_id = session()->get('UserID');

        if ($employee->update()) {
            return redirect()->back()->with('success', 'Employee Information Updated Successfully');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Employees::find($id)->delete()) {
            return back()->with('success', 'Employee has been deleted successfully');
        }
    }
}
