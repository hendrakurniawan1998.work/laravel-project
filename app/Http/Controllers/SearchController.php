<?php

namespace App\Http\Controllers;

use App\Models\Employees;
use App\Models\Companies;
use App\Models\SalesSummaries;
use App\Models\Sales;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    public function employeeFilter(Request $request, Employees $employee)
    {
        $employee_query = $employee->newQuery();

        // Search for a employee based on their first name.
        if ($request->has('employee_first_name') && $request->employee_first_name != null) {
            $employee_query->where('first_name', 'like', '%' . $request->input('employee_first_name') . '%');
            session()->put('employee_first_name', $request->employee_first_name);
        }

        // Search for a employee based on their company.
        if ($request->has('employee_company') && $request->employee_company != null) {

            $employee_query->where('company_id', $request->input('employee_company'));
            session()->put('employee_company', $request->employee_company);
        }

        // Search for a employee based on creation date picked by the user until now.
        if ($request->has('creation_date') && $request->creation_date != null) {
            $employee_query->whereBetween('created_at', [Carbon::parseFromLocale($request->input('creation_date')), Carbon::now('GMT+8')]);
            session()->put('employee_creation_date', $request->creation_date);
        }

        error_log("SESSION BEFORE PUT".json_encode(session("employee_first_name")));
        session()->put('employees', $employee_query->get());

        // Get the results and return them.
        return redirect()->back();
    }

    public function clearEmployeeFilter(Request $request)
    {
        session()->forget("employee_first_name");
        session()->forget("employees");
        session()->forget("employee_company");
        session()->forget("employee_creation_date");
        
        return redirect("employees");
    }

    public function companyFilter(Request $request, Companies $company)
    {
        $company_query = $company->newQuery();

        // Search for a employee based on their first name.
        if ($request->has('company_name') && $request->company_name != null) {
            $company_query->where('name', 'like', '%' . $request->input('company_name') . '%');
            session()->put("company_name", $request->company_name);
        }

        // Search for a company based on creation date picked by the user until now.
        if ($request->has('creation_date') && $request->creation_date != null) {
            $company_query->whereBetween('created_at', [Carbon::parseFromLocale($request->input('creation_date')), Carbon::now()]);
            session()->put("company_creation_date", $request->creation_date);
        }

        $request->session()->put('companies', $company_query->get());

        // Get the results and return them.
        return back();
    }

    public function clearCompanyFilter(Request $request)
    {
        session()->forget("company_name");
        session()->forget("companies");
        session()->forget("company_creation_date");
        return back();
    }


    public function summaryFilter(Request $request,  SalesSummaries $sales_summaries)
    {

        $summaries_query = $sales_summaries->newQuery();

        if ($request->has("summary_start_date") && $request->has("summary_end_date")) {
            if ($request->input("summary_start_date") != null && $request->input("summary_end_date") != null) {
                $summaries_query->whereBetween('date', [$request->input("summary_start_date"), $request->input("summary_end_date")]);
                session()->put("summary_start_date", $request->summary_start_date);
                session()->put("summary_end_date", $request->summary_end_date);
            }
        }

        if ($request->has("summary_company") && $request->summary_company != null) {
            $summaries_query
                ->join('employees', 'employees.id', '=', 'sales_summaries.employee_id')
                ->join('companies', 'companies.id', '=', 'employees.company_id')
                ->where("companies.id", "=", $request->input('summary_company'));

            session()->put("summary_company", $request->summary_company);
        }


        if ($request->has("summary_employee") && $request->summary_employee != null) {
            $summaries_query->where('employee_id', $request->input("summary_employee"));
            session()->put("summary_employee", $request->summary_employee);
        }

        $sales_summaries = $summaries_query->simplePaginate(10);

        return redirect("sales-summaries")->with(compact('sales_summaries'));
    }

    public function clearSummaryFilter(Request $request)
    {
        session()->forget("summary_start_date");
        session()->forget("summary_end_date");
        session()->forget("summary_company");
        session()->forget("summary_employee");

        $sales_summaries = SalesSummaries::simplePaginate(10);

        return back()->with(compact("sales_summaries"));
    }

    public function detail(Request $request, Sales $sale, $date)
    {

        $sales_query = $sale->newQuery();

        $sales_query->whereDate("sales.created_at", "=", $date);

        if (session()->has("summary_company")) {

            $sales_query
                ->join('employees', 'employees.id', '=', 'sales.employee_id')
                ->join('companies', 'companies.id', '=', 'employees.company_id')
                ->where("companies.id", "=", session('summary_company'));
        }

        if (session()->has("summary_employee")) {
            $sales_query->where('employee_id', session("summary_employee"));
        }

        $sales = $sales_query->get();

        return view('Sales.index', compact('sales'));
    }
}
