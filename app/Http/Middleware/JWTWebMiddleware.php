<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class JWTWebMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if ($request->session()->has('JWTAuthorization')) {

            error_log("JWT WEB MIDDLEWARE:".session()->get('JWTAuthorization'));
            $request->headers->set('Authorization', "Bearer " . session()->get('JWTAuthorization'));
        }

        return $next($request);
    }
}
