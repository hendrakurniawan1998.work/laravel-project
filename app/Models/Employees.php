<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Companies;

class Employees extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['first_name','last_name','phone','email',"company_id",'created_by_id','updated_by_id','created_at'];

    public function company()
    {
        return $this->belongsTo(Companies::class);
    }
}
