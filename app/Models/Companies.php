<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name','logo','website','email','created_at','created_by_id','updated_by_id'];

}
