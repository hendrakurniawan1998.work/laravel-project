<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    
    use HasFactory;

    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }

    public function item()
    {
        return $this->belongsTo(Items::class);
    }
}
