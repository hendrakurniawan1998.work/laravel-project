<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Companies;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class CompaniesControllerTest extends TestCase
{
    use DatabaseTransactions;



    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test */
    public function CRUDCompanyTest()
    {
        $company = Companies::create([
            "name" => "Test Company",
            "email" => "Test@gmail.com",
            "logo" => "Test",
            "website" => "test.com"
        ]);

        $this->assertDatabaseHas('companies', [
            "name" => "Test Company",
            "email" => "Test@gmail.com",
            "logo" => "Test",
            "website" => "test.com"
        ]);

        $updated_company = Companies::find($company->id)->update([
            "name" => "Test Company Update",
            "email" => "TestUpdate@gmail.com"
        ]);

        $this->assertDatabaseHas('companies', [
            "name" => "Test Company Update",
            "email" => "TestUpdate@gmail.com"
        ]);

        
        $deleted_company = Companies::destroy($company->id);

        $this->assertDatabaseMissing('companies', [
            "name" => "Test Company Update",
            "email" => "TestUpdate@gmail.com"
        ]);
    }
}
