<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Companies;
use App\Models\Employees;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class EmployeesControllerTest extends TestCase
{
    use DatabaseTransactions;

    /** @test */
    public function CRUDEmployeeTest()
    {
        $company = Companies::create([
            "name" => "Test Company",
            "email" => "Test@gmail.com",
            "logo" => "Test",
            "website" => "test.com"
        ]);

        $employee = Employees::create([
            "first_name" => "Hendra",
            "last_name" => "Kurniawan",
            "company_id" => $company->id,
            "email" => "hktest@gmail.com",
            "phone" => "98135288"
        ]);

        $this->assertDatabaseHas('employees', [
            "first_name" => "Hendra",
            "last_name" => "Kurniawan",
            "company_id" => $company->id,
            "email" => "hktest@gmail.com",
            "phone" => "98135288"
        ]);

        $updated_employee = Employees::find($employee->id)->update([
            "email" => "TestUpdate@gmail.com"
        ]);

        $this->assertDatabaseHas('employees', [
            "email" => "TestUpdate@gmail.com"
        ]);

        
        $deleted_employee = Employees::destroy($employee->id);

        $this->assertDatabaseMissing('employees', [
            "email" => "TestUpdate@gmail.com"
        ]);
    }
}
