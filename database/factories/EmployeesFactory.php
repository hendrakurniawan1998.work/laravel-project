$factory->define(App\Models\Employees::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'logo' => $faker->logo,
        'company_id' => Companies::inRandomOrder()->first()->id,
        'website' => $faker->website,
    ];
});