$factory->define(App\Models\Companies::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'logo' => $faker->logo,
        'website' => $faker->website,
    ];
});