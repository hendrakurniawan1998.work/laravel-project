<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class SaleSummarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_file = fopen(base_path("database/data/Sale Summary.csv"),"r");

        $firstline = true;

        while (($data = fgetcsv($csv_file, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                DB::table('sales_summaries')->insert([
                    "date" => $data['0'],
                    "employee_id" => $data['1'],
                    "price_total" => $data['2'],
                    "discount_total" => $data['3'],
                    "total" => $data['4'],
                    "created_date" => Carbon::now()->toDateTime(),
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csv_file);
    }
}
