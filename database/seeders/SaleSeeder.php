<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Sales;

class SaleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_file = fopen(base_path("database/data/Sale.csv"),"r");

        $firstline = true;

        while (($data = fgetcsv($csv_file, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Sales::create([
                    "created_at" => $data['0'],
                    "item_id" => $data['1'],
                    "price" => $data['2'],
                    "discount" => $data['3'],
                    "employee_id" => $data['4'],
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csv_file);
    }
}
