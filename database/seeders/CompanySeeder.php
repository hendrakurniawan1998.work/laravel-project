<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Companies;
use Illuminate\Support\Carbon;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_file = fopen(base_path("database/data/Company.csv"),"r");

        $firstline = true;

        while (($data = fgetcsv($csv_file, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Companies::create([
                    "name" => $data['0'],
                    "email" => $data['1'],
                    "logo" => $data['2'],
                    "website" => $data['3'],
                    "created_at" => Carbon::now(),
                    "updated_at" => null,
                    "created_by_id" => null,
                    "updated_by_id" => null,
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csv_file);
    }
}
