<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employees;
use Illuminate\Support\Carbon;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_file = fopen(base_path("database/data/Employee.csv"),"r");

        $firstline = true;

        while (($data = fgetcsv($csv_file, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Employees::create([
                    "first_name" => $data['0'],
                    "last_name" => $data['1'],
                    "company_id" => $data['2'],
                    "email" => $data['3'],
                    "phone" => $data['4'],
                    "created_at" => Carbon::now(),
                    "updated_at" => null,
                    "created_by_id" => null,
                    "updated_by_id" => null,
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csv_file);
    }
}
