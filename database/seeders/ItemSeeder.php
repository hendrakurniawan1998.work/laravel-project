<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Items;
use Illuminate\Support\Carbon;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv_file = fopen(base_path("database/data/Item.csv"),"r");

        $firstline = true;

        while (($data = fgetcsv($csv_file, 2000, ",")) !== FALSE) {
            if (!$firstline) {
                Items::create([
                    "name" => $data['0'],
                    "price" => $data['1'],
                    "created_at" => Carbon::now(),
                ]);    
            }
            $firstline = false;
        }
   
        fclose($csv_file);
    }
    
}
