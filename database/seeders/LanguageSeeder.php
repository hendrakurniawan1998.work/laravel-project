<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\TranslationLoader\LanguageLine;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('resources/lang/id.json'));

        $data = json_decode($jsonString, true);

        foreach($data as $datakey=>$datavalue) 
        {
            LanguageLine::create([
                'group' => 'translation',
                'key' => $datakey,
                'text' => ['id' => $datavalue, 'en' => $datakey],
             ]);
        }       
    }
}
