<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('employees', function (Blueprint $table) {

            $table->increments('employee_id');
            $table->string('first_name')->require();
            $table->string('last_name')->require();
            $table->integer('company')->unsigned();
            $table->foreign('company')->references('company_id')->on('companies');
            $table->string('email');
            $table->string('phone');
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
