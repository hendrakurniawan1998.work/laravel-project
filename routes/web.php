<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompaniesController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\TimezoneController;
use App\Http\Controllers\SalesSummariesController;
use App\Http\Controllers\ItemsController;
use App\Http\Controllers\SalesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Basic Routing

Route::get('/', function () {
    return view('welcome');
});



Route::get('/login', function () {
    return view('auth.login');
})->name('login');


Route::group([

    'middleware' => ['jwt.verify', 'prevent-back-history']

], function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');


    // Companies Route

    Route::get('/companies', [CompaniesController::class, 'index']);

    Route::get('/companies/create', [CompaniesController::class, 'create']);

    Route::post('/companies', [CompaniesController::class, 'store']);

    Route::get('/companies/edit/{id}', [CompaniesController::class, 'edit']);

    Route::post('/companies/update/{id}', [CompaniesController::class, 'update']);

    Route::get("/companies/delete/{id}", [CompaniesController::class, 'destroy']);


    // Employees Route

    Route::get('/employees', [EmployeesController::class, 'index']);

    Route::get('/employees/create', [EmployeesController::class, 'create']);

    Route::post('/employees', [EmployeesController::class, 'store']);

    Route::get('/employees/edit/{id}', [EmployeesController::class, 'edit']);

    Route::post('/employees/update/{id}', [EmployeesController::class, 'update']);

    Route::get("/employees/delete/{id}", [EmployeesController::class, 'destroy']);

    // Item Route

    Route::get('/items', [ItemsController::class, 'index']);

    Route::get('/items/create', [ItemsController::class, 'create']);

    Route::post('/items', [ItemsController::class, 'store']);

    Route::get('/items/edit/{id}', [ItemsController::class, 'edit']);

    Route::post('/items/update/{id}', [ItemsController::class, 'update']);

    Route::get("/items/delete/{id}", [ItemsController::class, 'destroy']);


    // Sales Route

    Route::get('/sales', [SalesController::class, 'index']);

    Route::get('/sales/create', [SalesController::class, 'create']);

    Route::post('/sales', [SalesController::class, 'store']);

    Route::get('/sales/edit/{id}', [SalesController::class, 'edit']);

    Route::post('/sales/update/{id}', [SalesController::class, 'update']);

    Route::get("/sales/delete/{id}", [SalesController::class, 'destroy']);

     // Sales Summaries Route

     Route::get('/daily-sales-summaries', [SalesSummariesController::class, 'daily']);

     Route::post('/daily-sales-summaries', [SalesSummariesController::class, 'daily']);

     Route::get('/sales-summaries', [SalesSummariesController::class, 'index']);



    // Localization Route

    Route::get('language/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);
        return back();
    })->name("language");

    // Queue Email Route

    Route::get('test-email', [JobController::class, 'enqueue']);

    // Search Filter Route
    Route::get('/clear-employees-filter', [SearchController::class, 'clearEmployeefilter']);

    Route::get('/clear-companies-filter', [SearchController::class, 'clearCompanyFilter']);

    Route::get('/clear-summaries-filter', [SearchController::class, 'clearSummaryFilter']);

    Route::post('/employees/filter', [SearchController::class, 'employeeFilter']);

    Route::post('/companies/filter', [SearchController::class, 'companyFilter']);

    Route::post('/sales-summaries/filter', [SearchController::class, 'summaryFilter']);

    Route::get('/sales-summaries/filter', [SearchController::class, 'summaryFilter']);

    Route::get('/sales-summaries/detail/{date}', [SearchController::class, 'detail']);



    // Timezone Route

    Route::post('/timezone', [TimezoneController::class, 'changeTime']);
});
