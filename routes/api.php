<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DataController; 
use App\Http\Controllers\AuthController;
use App\Http\Controllers\SupportAPIController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    
    'middleware' => 'api',

], function () {

    Route::post('/login', [AuthController::class, 'login'])->name('login-user');

    Route::post('/register', [AuthController::class, 'register'])->name('register');

    Route::post('/logout', [AuthController::class, 'logout'])->name('logout-user');

    Route::post('/support/retrieve-employee', [SupportAPIController::class, 'RetrieveEmployees']);

    Route::get('/logout', [AuthController::class, 'logout'])->name('logout-user');

    Route::post('/refresh', [UserController::class, 'refresh'])->name('refresh');

    Route::get('/user-profile', [UserController::class, 'userProfile']);

});
